import {
    products,
    cart,
    categories
} from './modules/db.js'

let cartLocalet = JSON.parse(localStorage.getItem('cart')) || cart

var swiper = new Swiper(".mySwiper", {
    pagination: {
        el: ".swiper-pagination",
        type: "fraction",
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    autoplay: {
        delay: 3000,
    }
});

console.log(swiper.navigation.nextEl);
const btnMenu = document.querySelector('.header-menu__icon')

const menuModal = document.querySelector('.header-modal__widow')
const menuModalBg = document.querySelector('.header-modal__widow__bg')
const menuClosers = document.querySelectorAll('.menu__closer')

function openModal(modal, modal__bg) {
    modal__bg.style.display = 'block'
    modal.style.left = '0'
    setTimeout(() => {
        modal__bg.style.opacity = '1'
    }, 200)
}

function closeModal(modal, modal__bg) {
    modal.style.left = '-100%'
    modal__bg.style.opacity = '0'
    setTimeout(() => {
        modal__bg.style.display = 'none'
    }, 200)
}
btnMenu.onclick = () => {
    openModal(menuModal, menuModalBg)
}
menuClosers.forEach(closer => {
    closer.onclick = () => {
        closeModal(menuModal, menuModalBg)
    }
})
let footerEmailInput = document.querySelector('.footer-email__input')
let orangeLine = document.querySelector('.orangeLine')
footerEmailInput.onfocus = () => {
    orangeLine.style.width = '98%'
}
footerEmailInput.onblur = () => {
    orangeLine.style.width = '0%'
}
const accardeonBtn = document.querySelector('.showAccar')
const arrowDown = document.querySelector('.arrow')
const accInnerBtn = document.querySelector('.footer-accardeon__closer')
const footAccardeon = document.querySelector('.accardeon')
accardeonBtn.onclick = () => {
    if (footAccardeon.classList.contains('footer-site__info-hidden')) {
        footAccardeon.classList.add('footer-site__info-shown')
        footAccardeon.classList.remove('footer-site__info-hidden')
        arrowDown.style.transform = 'rotate(0deg)'
    } else {
        arrowDown.style.transform = 'rotate(180deg)'
        footAccardeon.classList.remove('footer-site__info-shown')
        footAccardeon.classList.add('footer-site__info-hidden')
    }
}
accInnerBtn.onclick = () => {
    arrowDown.style.transform = 'rotate(180deg)'
    footAccardeon.classList.remove('footer-site__info-shown')
    footAccardeon.classList.add('footer-site__info-hidden')
}


// -- main_t  --  //

// let [elem, elem2, elem3, elem4,] = products

let mainT = document.querySelector('.main_bottom')

function reloadProducts(arr) {
    for (let elem of arr) {

        mainT.innerHTML += `
            <div class="bottom_box product_name " >
                <div class="top">
                <div class="box_icon">
                    <div class="left"></div>
                    <div class="right" id="${elem.id}" >
                        <img src="./assets/icons/4115237_add_plus_icon.svg" width="34px" alt="">
                    </div>
                </div>
                <div class="box_img" style="background-image: url('./assets/img/${elem.img}');" ></div>
                <div class="box"></div>
                </div>
                <div class="bottom">
                <p class="product">${elem.name}</p>
                <p class="price">${elem.price} тыс. р. ${elem.isNew ? '<span class="new_promo">Новинка</span>' : ''}</p>
                </div>
            </div>
        `
    }
    let cartBtns = document.querySelectorAll('.top .right')

    cartBtns.forEach(element => {
        element.onclick = () => {
            cartLocalet.push(element.id)
            localStorage.setItem('cart', JSON.stringify(cartLocalet))
        }
    });

}

reloadProducts(products)


let tuda_p = document.querySelector('.tuda_p')
let syda_p = document.querySelector('.syda_p')

let father_one = document.querySelector('.father')
let father_two = document.querySelector('.father_two')

tuda_p.onclick = () => {
    tuda_p.style.color = "white"
    tuda_p.style.borderBottom = '4px solid orange'

    syda_p.style.color = '#9D9390'
    syda_p.style.borderBottom = 'none'

    father_one.style.width = '100%'
    father_one.style.backgroundColor = '#3A3330'
    father_one.style.display = 'flex'
    father_one.style.alignItems = 'center'

    father_two.style.display = 'none'
}

syda_p.onclick = () => {
    syda_p.style.color = "white"
    syda_p.style.borderBottom = '4px solid orange'

    tuda_p.style.color = '#9D9390'
    tuda_p.style.borderBottom = 'none'

    father_two.style.width = '100%'
    father_two.style.display = 'flex'
    father_two.style.alignItems = 'center'
    father_two.style.paddingBottom = '100px'
    father_two.style.paddingTop = '100px'

    father_one.style.display = 'none'
}

import {
    novelties,
    popular
} from './modules/db_for_swip.js'

let son_one = document.querySelector('.son')
let son_two = document.querySelector('.son_two')
let box = document.querySelectorAll('.box')


function firts_swip(arr) {
    son_one.innerHTML = ''

    for (let item of arr) {
        son_one.innerHTML += `
        <div class="swiper-slide box">
        <div class="swip_img" style="background-image: url('./assets/img/${item.img}');" ></div>
                            <div class="box_top">
                                <div class="name_logo">
                                    <img src="./assets/icons/name_logo_aziz.svg">
                                </div>
                                <div class="ball"></div>
                                <div class="add">
                                    <img src="./assets/icons/add_aziz.svg">
                                </div>
                                <div class="buy">
                                    <img src="./assets/icons/buy_aziz.svg">
                                </div>
                            </div>
                            <div class="box_bottom">
                            <p class="name">${item.name}</p>
                                <div class="for_price">
                                    <p class="price">${item.price} тыч. р.</p>
                                    ${item.isNew ? '<p class="new">Новинка</p>' : ''}
                                </div>
                            </div>
        </div>
        `
    }
}

firts_swip(novelties)

function second_swip(arr) {
    son_two.innerHTML = ''

    for (let item of arr) {
        son_two.innerHTML += `
        <div class="swiper-slide box">
        <div class="swip_img" style="background-image: url('./assets/img/${item.img}');" ></div>
                            <div class="box_top">
                                <div class="name_logo">
                                    <img src="./assets/icons/name_logo_aziz.svg">
                                </div>
                                <div class="ball"></div>
                                <div class="add">
                                    <img src="./assets/icons/add_aziz.svg">
                                </div>
                                <div class="buy">
                                    <img src="./assets/icons/buy_aziz.svg">
                                </div>
                            </div>
                            <div class="box_bottom">
                            <p class="name">${item.name}</p>
                                <div class="for_price">
                                    <p class="price">${item.price} тыч. р.</p>
                                    ${item.isNew ? '<p class="new">Новинка</p>' : ''}
                                </div>
                            </div>
        </div>
        `
    }
}

second_swip(popular)

function animation() {
    box.forEach(item => {
        item.onmouseenter = () => {
            item.classList.toggle('box_post')
            let ball = item.querySelector('.ball')
            ball.classList.toggle('ball_post')

            let add = item.querySelector('.add')
            let buy = item.querySelector('.buy')
            add.ondblclick = () => {
                ball.classList.toggle('ball_cost')
                add.classList.toggle('add_post')
                buy.classList.toggle('buy_post')
            }
            add.ondblclick = () => {
                ball.classList.toggle('ball_cost')
                add.classList.toggle('add_post')
                buy.classList.toggle('buy_post')
            }
        }
        item.onmouseleave = () => {
            item.classList.toggle('box_post')
            let ball = item.querySelector('.ball')
            ball.classList.toggle('ball_post')

            let add = item.querySelector('.add')
            let buy = item.querySelector('.buy')
            add.ondblclick = () => {
                ball.classList.toggle('ball_cost')
                buy.classList.toggle('buy_post')
            }
            add.ondblclick = () => {
                ball.classList.toggle('ball_cost')
                buy.classList.toggle('buy_post')
            }
        }
    })
}
animation()


const backs = [
    {
        bg: 'modal-first__bg.jpg'
    },
    {
        bg: 'modal-second__bg.jpg'
    },
    {
        bg: 'modal-third__bg.jpg'
    },
    {
        bg: 'modal-fourth__bg.jpg'
    },
    {
        bg: 'modal-fivth__bg.jpg'
    },
    {
        bg: 'modal-sixth__bg.jpg'
    },
    {
        bg: 'example.jpg'
    },
]
const modalBtns = document.querySelectorAll('.header-category__item')
modalBtns.forEach((btn, idx) => {
    btn.onmouseenter = () => {
        // menuModal.style.backgroundImage = ``
        menuModal.style.backgroundImage = `url('../assets/img/${backs[idx].bg}')`
    }
})

// modal

let search_div = document.querySelector('.search')
let search_bg = document.querySelector('.search_bg')
let icon = document.querySelector('.header-search__icon')

function openSearch() {
    icon.onclick = () => {
        search_div.style.display = "block"
        close_search.style.display = "block"
        search_bg.style.display = "block"

        setTimeout(() => {
            search_div.style.opacity = "1"
            search_div.style.transform = "translate(-50%, -50%) scale(1)"
            close_search.style.opacity = "1"
            close_search.style.transform = "translate(-50%, -50%) scale(1)"
            search_bg.style.opacity = "1"
        }, 300);
    }
}
openSearch()

let close_search = document.querySelector('.close_search')

close_search.onclick = () => {
    closeSearch()
}

function closeSearch() {
    search_div.style.transform = "translate(-50%, -50%) scale(.2)"
    search_div.style.opacity = "0"
    close_search.style.opacity = "0"
    search_bg.style.opacity = "0"

    setTimeout(() => {
        search_div.style.display = "none"
        close_search.style.display = "none"
        search_bg.style.display = "none"
    }, 300);
}

let help_div_one = document.querySelector('.help_div_one')
let help_div_two = document.querySelector('.help_div_two')

let search = document.querySelector('.sea_inp')
search.onkeyup = () => {
    let filtered = products.filter(item => item.name.toLowerCase().includes(search.value.toLowerCase().trim()))
    reload(filtered)
    search_product(filtered)
}

reload(products)
function reload(arr) {
    help_div_one.innerHTML = ''

    for (let item of arr) {
        help_div_one.innerHTML += `
        <p class="help_text">${item.name}</p>
        `
    }
}

function help_two(arr) {
    help_div_two.innerHTML = ''

    for (let item of arr) {
        help_div_two.innerHTML += `
        <p class="help_text">${item.name}</p>
        `
    }
}
help_two(categories)

let grids_three = document.querySelector('.grids_three')
search_product(products)

function search_product(arr) {
    grids_three.innerHTML = ''

    for (let item of arr) {
        grids_three.innerHTML += `
    <div class="itemchik">
    <div class="itemchik_img" style="background-image: url('../assets/img/${item.img}');" ></div>
        <div class="itemchik_ball">
            <img src="./assets/icons/add_aziz.svg" class="ball_img">
        </div>
        <div class="betta">
            <img src="./assets/icons/yes.png" class="ball_img">
        </div>
        <p class="itemchik_p">${item.name}</p>
    </div>
        `

        let itemchik = document.querySelector('.itemchik')
    }
}
