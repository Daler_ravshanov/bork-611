import {
    products, 
    cart
} from './db.js'
import {
    getAllData
} from './FUNCTIONS.JS'

let currentCart = JSON.parse(localStorage.getItem('cart')) || cart
if (currentCart.length <= 0) {
    window.location.href = '../index.html'
}

let korzin = document.querySelector('.item__box')
const rowContainer = document.querySelector('.item__collection')
const rwContPurchaseBar = document.querySelector('.item__row-descrption')

function otr(item) {
    korzin.innerHTML = ``
    korzin.innerHTML += `
        <div class="item__box-screen">
        <button class="item__box-deleter">×</button>
        <img src="../assets/img/${item.img}">
        <h2 class="item__box-title">${item.name}</h2>
    </div>
    <div class="item__box-description">
        <div class="empty"></div>
        <form>
            <select class="select" >
                <option value="${item.qt}">${item.qt}</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
        </form>
        <div class="item__box-purchase">
            <div class="item__box-price">
                <span>К оплате:</span>
                <span>${item.price * item.qt} тыч. р.</span>
            </div>
            <a href="./arrange.html">Перейти к оформлению</a>
        </div>
    </div>
        `
    let select = document.querySelector('.select')
    let option = new Option(item.qt, item.qt, true)

    select.append(option)
}

function showRowItems(arr, cont) {
    let genCost = document.querySelector('.item__row-all__price')
    let total = 0
    cont.innerHTML = '';
    for (let item of arr) {
        item.total = item.price * item.qt
        cont.innerHTML += `
            <div class="item__row">
                <div class="item__row-left__side">
                    <img src="../assets/img/${item.img}" alt="">
                    <h2>${item.name}</h2>
                </div>
                <div class="item__row-right__side">
                    <form>
                        <select class="select" >
                            <option value="${item.qt}">${item.qt}</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </form>
                    <span>${item.total}</span>
                    <button id="${item.id}" class="item__box-deleter">×</button>
                </div>
            </div>
        `
        total += item.total
    }
    genCost.innerHTML = `${total} тыс. руб.`
}

if (currentCart.length > 1) {
    korzin.style.display = 'none'
    rowContainer.style.display = 'flex'
    rwContPurchaseBar.style.display = 'flex'
    showRowItems(getAllData(products, cart), rowContainer)
    showRowItems(getAllData(products, currentCart), rowContainer)
} else {
    rwContPurchaseBar.style.display = 'none'
    otr(getAllData(products, currentCart)[0])
    korzin.style.display = 'flex'
    rowContainer.style.display = 'none'
    
}
console.log(cart);

//////

// let left_m = document.querySelector('.left_m')
// let tovari = document.querySelector('.tovari')
// let dengi = document.querySelector('.dengi')

// function oplata(arr) {
//     left_m.innerHTML += ''

//     for(let item of arr){
//         left_m.innerHTML = `
//         <div class="left_m__img" style="background-image: url('./assets/img/${item.img}');" ></div>
//         `
//         tovari.innerHTML = `Ваш заказ : ${item.name}` 
//         dengi.innerHTML = `К оплате : ${item.price}`
//     }
// }
// }