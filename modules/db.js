export let products = [{
        id: 1,
        name: "Угольный гриль BORK G503",
        qt: 0,
        price: 15000,
        img: 'G503.png',
        description: 'Аксессуары для выдержанных красных, насыщенных розовых и винтажных шампанских вин',
        isNew: false
    },
    {
        id: 2,
        name: "Кислородный декантер Z600",
        qt: 0,
        price: 49000,
        img: 'Z600.png',
        description: 'Система нового поколения для раскрытия вкуса и аромата напитков',
        isNew: true
    },
    {
        id: 3,
        name: "Массажер для глаз D617",
        qt: 0,
        price: 16000,
        img: 'K810.png',
        description: 'Первая модель BORK с сохранением обзора во время сеанса',
        isNew: true,
    },
    {
        id: 4,
        name: "Соковыжиматель S703",
        qt: 0,
        price: 42000,
        img: 'Z631.png',
        description: 'Мощная система в стальном корпусе для эффективного отжима сока с кувшином для хранения',
        isNew: false
    },
]

export let cart = [

]

export let categories = [{
        id: 1,
        name: "Чайники",
        qt: 0,
        price: 15000,
        img: 'G503.png',
        description: 'Аксессуары для выдержанных красных, насыщенных розовых и винтажных шампанских вин',
        isNew: false
    },
    {
        id: 2,
        name: "Мультиварки",
        qt: 0,
        price: 49000,
        img: 'Z600.png',
        description: 'Система нового поколения для раскрытия вкуса и аромата напитков',
        isNew: true
    },
    {
        id: 3,
        name: "Соковыжиматели",
        qt: 0,
        price: 42000,
        img: 'Z631.png',
        description: 'Мощная система в стальном корпусе для эффективного отжима сока с кувшином для хранения',
        isNew: false
    },
    {
        id: 4,
        name: "Мойки воздуха",
        qt: 0,
        price: 16000,
        img: 'K810.png',
        description: 'Первая модель BORK с сохранением обзора во время сеанса',
        isNew: true,
    },
]